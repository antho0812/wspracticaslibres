<?php
	include ("../Connection.php");
	require "../Utils/RepuestaDimanica.php";

	$respuesta = new RespuestaDinamica();
	$utils = new Utils();

	$accion = $_GET["accion"];
	if($accion == "insertar"){

		$horariopractica = json_decode($_GET["horariopractica"]);

		$query = "INSERT INTO `horario_practica_libre`(`idLaboratorio`, `idTipoProgramacion`, `CuposReservados`, `FechaInicio`, `FechaFin`, `HoraInicio`, `HoraFin`, `Estado`) 
		VALUES ('$horariopractica->idLaboratorio','$horariopractica->idTipoProgramacion',
		'$horariopractica->cuposReservados','$horariopractica->fecha_Inicio',
		'$horariopractica->fecha_Fin','$horariopractica->hora_Incio',
		'$horariopractica->hora_Fin','$horariopractica->estado')";

		echo $query;
		
		$resultado = $conn->query($query);

		if(!$resultado){
			$respuesta->setMensajeError();
		}
		
		echo json_encode($respuesta);

	}else if($accion == "actualizar"){

		$horariopractica = json_decode($_GET["horariopractica"]);

		$resultado = $conn->query("UPDATE `horario_practica_libre` SET `idLaboratorio`='$horariopractica->idLaboratorio',`idTipoProgramacion`='$horariopractica->idTipoProgramacion',
		`CuposReservados`='$horariopractica->cuposReservados',`FechaInicio`='$horariopractica->fecha_Inicio',
		`FechaFin`='$horariopractica->fecha_Fin',`HoraInicio`='$horariopractica->hora_Incio',
		`HoraFin`='$horariopractica->hora_Fin',`Estado`='$horariopractica->estado'
		 WHERE idHorarioPracticaLibre = '$horariopractica->idHorarioPracticaLibre'");
		
		echo json_encode($respuesta);

	}else if($accion == "buscar"){

		$id = $_GET["id"];
		$query = "SELECT * FROM `horario_practica_libre` where idHorarioPracticaLibre = " . $id;
		$resultado = $conn->query($query);	
		$row = mysqli_fetch_assoc($resultado);

		if(isset($row)){
			echo json_encode($row);
		}else{
			echo json_encode("");
		}
		
	}else if($accion == "todos"){

		$query = "SELECT * FROM `horario_practica_libre`";

		if(isset($_GET["where"])){
			$where = json_decode($_GET["where"]);
			$query = $query . " where " . $where->where;	
		}
		
		$resultado = $conn->query($query);	
		echo $utils->obtenerListaComoJson($resultado);

	}

?>