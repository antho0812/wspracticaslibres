<?php
	include ("../Connection.php");
	require "../Utils/RepuestaDimanica.php";

	$respuesta = new RespuestaDinamica();
	$utils = new Utils();

	$accion = $_GET["accion"];
	if($accion == "insertar"){

		$edificio = json_decode($_GET["edificio"]);

		$resultado = $conn->query("INSERT INTO `edificio`(`Nombre`, `Direccion`, `Telefono`) 
		VALUES ('$edificio->nombre','$edificio->direccion','$edificio->telefono')");

		if(!$resultado){
			$respuesta->setMensajeError();
		}
		
		echo json_encode($respuesta);

	}else if($accion == "actualizar"){

		$edificio = json_decode($_GET["edificio"]);

		$resultado = $conn->query("UPDATE `edificio` SET 
		`Nombre`='$edificio->nombre',`Direccion`='$edificio->direccion',`Telefono`='$edificio->telefono'
		 WHERE idEdificios = '$edificio->idEdificios'");

		if(!$resultado){
			$respuesta->setMensajeError();
		}
		
		echo json_encode($respuesta);

	}else if($accion == "buscar"){

		$id = $_GET["id"];
		$query = "SELECT * FROM `edificio` where idEdificios = " . $id;
		$resultado = $conn->query($query);	
		$row = mysqli_fetch_assoc($resultado);

		if(isset($row)){
			echo json_encode($row);
		}else{
			echo json_encode("");
		}
		
	}else if($accion == "todos"){

		$query = "SELECT * FROM `edificio`";

		if(isset($_GET["where"])){
			$where = json_decode($_GET["where"]);
			$query = $query . " where " . $where->where;	
		}
		
		$resultado = $conn->query($query);	
		echo $utils->obtenerListaComoJson($resultado);

	}

?>