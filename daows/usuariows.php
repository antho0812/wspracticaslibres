<?php
	include ("../Connection.php");
	require "../Utils/RepuestaDimanica.php";

	$respuesta = new RespuestaDinamica();
	$utils = new Utils();

	$accion = $_GET["accion"];
	if($accion == "insertar"){

		$usuario = json_decode($_GET["usuario"]);

		$query = "INSERT INTO `usuario`(`idTipoUsuario`, `Nombre`, `Apellido`, `Email`, `Password`, `Direccion`, `Telefono`, `Estado`) 
		VALUES ('$usuario->idTipoUsuario',
		'$usuario->nombre',
		'$usuario->apellido',
		'$usuario->email',
		'$usuario->password',
		'$usuario->direccion',
		'$usuario->telefono',
		'$usuario->estado')";

		$resultado = $conn->query($query);
		
		
		if(!$resultado){
			$respuesta->setMensajeError();
		}
		
		echo json_encode($respuesta);

	}else if($accion == "actualizar"){

		$usuario = json_decode($_GET["usuario"]);

		$resultado = $conn->query("UPDATE `usuario` SET
		`idTipoUsuario`='$usuario->idTipoUsuario',
		`Nombre`= '$usuario->nombre',
		`Apellido`='$usuario->apellido',
		`Email`= '$usuario->email',
		`Password`= '$usuario->password',
		`Direccion`= '$usuario->direccion',
		`Telefono`= '$usuario->telefono',
		`Estado`='$usuario->estado' WHERE idUsuario = $usuario->idUsuario");

		if(!$resultado){
			$respuesta->setMensajeError();
		}
		
		echo json_encode($respuesta);

	}else if($accion == "buscar"){

		$id = $_GET["id"];
		$query = "SELECT * FROM `usuario` where idUsuario = " . $id;
		$resultado = $conn->query($query);	
		$row = mysqli_fetch_assoc($resultado);

		if(isset($row)){
			echo json_encode($row);
		}else{
			echo json_encode("");
		}
		
	}else if($accion == "todos"){

		$query = "SELECT * FROM `usuario`";

		if(isset($_GET["where"])){
			$where = json_decode($_GET["where"]);
			$query = $query . " where " . $where->where;	
		}
		
		$resultado = $conn->query($query);	
		echo $utils->obtenerListaComoJson($resultado);

	}

?>