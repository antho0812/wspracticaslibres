<?php
	include ("../Connection.php");
	require "../Utils/RepuestaDimanica.php";

	$respuesta = new RespuestaDinamica();
	$utils = new Utils();

	$accion = $_GET["accion"];
	if($accion == "insertar"){

		$laboratorio = json_decode($_GET["laboratorio"]);

		$resultado = $conn->query("INSERT INTO `laboratorio`( `idUsuario`, `idEdificio`, `Nombre`, `Capacidad`, `Telefono`, `Nivel`) 
		VALUES ('$laboratorio->idUsuario','$laboratorio->idEdificio',
		'$laboratorio->nombre','$laboratorio->capacidad',
		'$laboratorio->telefono','$laboratorio->nivel')");

		if(!$resultado){
			$respuesta->setMensajeError();
		}
		
		echo json_encode($respuesta);

	}else if($accion == "actualizar"){

		$laboratorio = json_decode($_GET["laboratorio"]);
		$resultado = $conn->query("UPDATE `laboratorio` SET `idUsuario`='$laboratorio->idUsuario',
		`idEdificio`='$laboratorio->idEdificio',`Nombre`='$laboratorio->nombre',
		`Capacidad`='$laboratorio->capacidad',`Telefono`='$laboratorio->telefono',
		`Nivel`='$laboratorio->nivel' WHERE idLaboratorio = '$laboratorio->idLaboratorio'");

		if(!$resultado){
			$respuesta->setMensajeError();
		}
		
		echo json_encode($respuesta);

	}else if($accion == "buscar"){

		$id = $_GET["id"];
		$query = "SELECT * FROM `laboratorio` where idLaboratorio = " . $id;
		$resultado = $conn->query($query);	
		$row = mysqli_fetch_assoc($resultado);

		if(isset($row)){
			echo json_encode($row);
		}else{
			echo json_encode("");
		}
		
	}else if($accion == "todos"){

		$query = "SELECT * FROM `laboratorio`";

		if(isset($_GET["where"])){
			$where = json_decode($_GET["where"]);
			$query = $query . " where " . $where->where;	
		}
		
		$resultado = $conn->query($query);	
		echo $utils->obtenerListaComoJson($resultado);

	}

?>