<?php
	include ("../Connection.php");
	require "../Utils/RepuestaDimanica.php";

	$respuesta = new RespuestaDinamica();
	$utils = new Utils();

	$accion = $_GET["accion"];
	if($accion == "insertar"){

		$tipoprogramacion = json_decode($_GET["tipoprogramacion"]);

		$resultado = $conn->query("INSERT INTO `tipoprogramacion`( `TipoProgramacion`, `Estado`) 
		VALUES ('$tipoprogramacion->tipoProgramacion','$tipoprogramacion->estado')");

		if(!$resultado){
			$respuesta->setMensajeError();
		}
		
		echo json_encode($respuesta);

	}else if($accion == "actualizar"){

		$tipoprogramacion = json_decode($_GET["tipoprogramacion"]);

		$resultado = $conn->query("UPDATE `tipoprogramacion` SET 
		`TipoProgramacion`='$tipoprogramacion->tipoProgramacion',`Estado`='$tipoprogramacion->estado'
		WHERE idTipoProgramacion = '$tipoprogramacion->idTipoProgramacion'");

		if(!$resultado){
			$respuesta->setMensajeError();
		}
		
		echo json_encode($respuesta);

	}else if($accion == "buscar"){

		$id = $_GET["id"];
		$query = "SELECT * FROM `tipoprogramacion` where idTipoProgramacion = " . $id;
		$resultado = $conn->query($query);	
		$row = mysqli_fetch_assoc($resultado);

		if(isset($row)){
			echo json_encode($row);
		}else{
			echo json_encode("");
		}
		
	}else if($accion == "todos"){

		$query = "SELECT * FROM `tipoprogramacion`";

		if(isset($_GET["where"])){
			$where = json_decode($_GET["where"]);
			$query = $query . " where " . $where->where;	
		}
		
		$resultado = $conn->query($query);	
		echo $utils->obtenerListaComoJson($resultado);

	}

?>