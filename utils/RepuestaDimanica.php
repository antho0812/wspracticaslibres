<?php
class RespuestaDinamica{
	public $codigo = "0000";
	public $descripcion = "Proceso realizado correctamente";

	public function setMensajeError(){
		$this->codigo = "9999";
		$this->descripcion = "HA OCURRIDO UN ERROR";
	}
}

class Utils{
	public function obtenerListaComoJson($resultado){
		$jsonArray =  array();

		while($fila = mysqli_fetch_assoc($resultado)){
			$jsonArray[] = $fila;
		}

		return json_encode($jsonArray);
	}
}
?>